#ifndef PORTABILITE_H
#define PORTABILITE_H

#if defined(_WIN16) || defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(__TOS_WIN__) || defined(__WINDOWS__) || defined(_WIN32_WCE) \
	|| defined(WIN32_PLATFORM_HPC2000) || defined(WIN32_PLATFORM_HPCPRO) || defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
	#ifndef WINDOWS
		#define WINDOWS
	#endif // WINDOWS
#elif defined(__linux__) || defined(linux) || defined(__linux) || defined(__gnu_linux__)
	#ifndef LINUX
		#define LINUX
	#endif // LINUX
#endif

// Code source suivant copié du site https://broux.developpez.com/articles/c/sockets/#LII-C

#ifdef WINDOWS /* si vous êtes sous Windows */
	#include <winsock2.h>
	
	#define close(s) closesocket(s)
	
	typedef int socklen_t;
#elif defined (LINUX) /* si vous êtes sous Linux */
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <unistd.h> /* close */
	#include <netdb.h> /* gethostbyname */
	
	#define INVALID_SOCKET -1
	#define SOCKET_ERROR -1
	
	typedef int SOCKET;
	typedef struct sockaddr_in SOCKADDR_IN;
	typedef struct sockaddr SOCKADDR;
	typedef struct in_addr IN_ADDR;
#else /* sinon vous êtes sur une plateforme non supportée */
	#error not defined for this platform
#endif

static inline void init(void)
{
	#ifdef WINDOWS
		WSADATA wsa;
		int err = WSAStartup(MAKEWORD(2, 2), &wsa);
		if(err < 0)
		{
			puts("WSAStartup failed !");
			exit(EXIT_FAILURE);
		}
	#endif
}

static inline void end(void)
{
	#ifdef WINDOWS
		WSACleanup();
	#endif
}

#include <string.h>
#include <errno.h>

static inline void gestionErreurs(char const* messageErreur/*, int en*/)
{
	/*if(en == 0)
		fprintf(stderr, "%s Erreur du systeme : %s.\n", messageErreur, strerror(errno));
	else if(en == 1)
		fprintf(stderr, "%s Erreur du systeme : %s.\n", messageErreur, strerror(errno));
	else
		fprintf(stderr, "NEVER GONNA GIVE YOU UP !\n");*/
	
	fprintf(stderr, "%s Erreur du systeme : %s.\n", messageErreur, strerror(errno));
	
	exit(1);
}

#endif // PORTABILITE_H
