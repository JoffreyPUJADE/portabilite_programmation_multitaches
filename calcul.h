#ifndef CALCUL_H
#define CALCUL_H

// Définition des macros pour détecter l'OS.

#if defined(_WIN16) || defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(__TOS_WIN__) || defined(__WINDOWS__) || defined(_WIN32_WCE) \
	|| defined(WIN32_PLATFORM_HPC2000) || defined(WIN32_PLATFORM_HPCPRO) || defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
	#ifndef WINDOWS
		#define WINDOWS
	#endif // WINDOWS
#elif defined(__linux__) || defined(linux) || defined(__linux) || defined(__gnu_linux__)
	#ifndef LINUX
		#define LINUX
	#endif // LINUX
#endif

// Définition des macros pour détecter le langage.

/*
	__STDC_VERSION__ = 199409L : C94
	__STDC_VERSION__ = 199901L : C99
	__STDC_VERSION__ = 201112L : C11
	__STDC_VERSION__ = 201710L : C18
	
	__cplusplus = 199711L : C++98
	__cplusplus = 201103L : C++11
	__cplusplus = 201402L : C++14
	__cplusplus = 201703L : C++17
	
	Pour la norme C++98, une mise à jour est à prévoir sous certains compilateurs. Pour en savoir plus, allez voir le site suivant : https://sourceforge.net/p/predef/wiki/Standards/
*/

#if defined(__STDC__) || (defined(__STDC_VERSION__) && ((__STDC_VERSION__ >= 199409L) && (__STDC_VERSION__ <= 201710L)))
	#ifndef C_LANGUAGE
		#define C_LANGUAGE
	#endif // C_LANGUAGE
#elif (defined(__cplusplus) && ((__cplusplus >= 199711L) && (__cplusplus <= 201703L))) || (defined(__cplusplus_cli) && (__cplusplus_cli == 200406L)) || defined(__embedded_cplusplus)
	#ifndef CPP_LANGUAGE
		#define CPP_LANGUAGE
	#endif // CPP_LANGUAGE
#endif

#if defined(WINDOWS)
	#include <windows.h>
#elif defined(LINUX)
	#include <unistd.h>
#else
	#error "La portabilité n\'est pas définie pour cette plateforme."
#endif

typedef unsigned int uint;
typedef long unsigned int luint;

static inline void sleeping(uint sec)
{
	#if defined(WINDOWS)
		sec *= 1000;
		
		#if defined(CPP_LANGUAGE)
			Sleep(static_cast<luint>(sec));
		#elif defined(C_LANGUAGE)
			Sleep(((uint)sec));
		#endif
	#elif defined(LINUX)
		sleep(sec);
	#endif
}

static inline void calcul(uint sec)
{
	sleeping(3 * sec);
}

#endif // CALCUL_H
