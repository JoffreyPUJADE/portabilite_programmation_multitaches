# Portabilite Programmation Multitaches

Ce dépôt contient tous les fichiers nécessaires pour réaliser au maximum la portabilité des codes sources de l'UE de Programmation Multitâches entre Windows et Linux.

# Sommaire

* [Avertissement](#avertissement)
* [Fichiers](#fichiers)
	* [Portabilite.h](#portabiliteh)
	* [calcul.h](#calculh)
* [Limites d'utilisation](#limites-dutilisation)
* [Utilisation de PThread sous Windows](#utilisation-de-pthread-sous-windows)
	* [Compilation avec PThread sous Windows](#compilation-avec-pthread-sous-windows)

# Avertissement

Le code source des différents fichiers de ce dépôt est amené à être modifié par la suite, afin de répondre au mieux aux besoins des différents TPs de l'UE de Programmation Multitâches.

Sachant cela, je vous invite fortement à revenir régulièrement sur ce dépôt git, afin de voir si les fichiers sources que vous avez ont été modifiés, et/ou si des instructions de ce README ont changé, afin de vous permettre d'utiliser au mieux les fichiers sources.

# Fichiers

## Portabilite.h

Ce fichier permet la portabilité entre Windows et Linux pour les TP de la partie réseaux.

## calcul.h

Ce fichier permet d'émuler la fonction `void calcul(int sec);` fourni par la prof en version header-only, et de la porter sous Linux et Windows.

Pour l'utiliser, il n'y a qu'à inclure ce fichier, plus besoin des fichiers `.o` de la prof avec ce fichier.

Il est possible qu'il y ait besoin du flag de compilation `-lkernel32` lors de l'édition des liens sous Windows. Je n'ai pas encore testé cela. EDIT : Après test sous Windows, ce flag de compilation n'est pas nécessaire.

# Limites d'utilisation

La portabilité que permet ces différents fichiers est limitée.

La fonction `fork()` est uniquement disponible sous Linux. Je n'ai pas réussi à réaliser un portage sous Windows, elle est donc non portable via mes fichiers de portabilité et utilisable uniquement sous Linux.

# Utilisation de PThread sous Windows

Il est possible d'utiliser la librairie "PThread" (utilisée en cours) sous Windows.

Je conseille alors d'utiliser le compilateur MinGW et, lors de son installation, vérifiez bien que la librairie PThread est installée avec. Sinon, il faut l'installer par vous-même, internet doit sûrement regorger de renseignements pour vous aider à cela. Si d'aventures, vous avez un doute sur le fait que la librairie PThread soit installée ou non sur votre machine, vous n'avez qu'à essayer de compiler un projet l'utilisant.

Je n'ai pas utilisé la librairie PThread sous Windows avec d'autres compilateurs que MinGW, donc je ne sais pas si ça marchera si vous avez un compilateur différent. Néanmoins, vous aurez sûrement la réponse sur internet ou en demandant à des personnes qui ont déjà essayé.

## Compilation avec PThread sous Windows

Afin de compiler un projet utilisant la librairie PThread sous Windows, vous devez utiliser, durant toutes les étapes de la compilation, le flag de compilation `-lpthread`, du moins, avec MinGW (je n'ai pas essayé avec d'autres compilateurs).